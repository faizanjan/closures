function counterFactory(initialCount=0) {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.

    let counter = typeof(initialCount)==='number'? initialCount : 0;

    const handleCounter = {
        increment: ()=>{
            return ++counter;
        },
        decrement : ()=>{
            return --counter;
        }
    }

    return handleCounter;
}

module.exports = counterFactory;