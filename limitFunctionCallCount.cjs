function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned

    if(typeof(cb)!=='function' || typeof(n)!=="number") {
        throw new Error("Invalid Parameters");
    }

    let callsLeft = n;
    const callCB = (...arg)=>{
        if(callsLeft>0) {
            callsLeft--;
            return cb(...arg);
        }
        else {
            console.warn("You have exhausted the limit of calls you can make");
            return null;
        }
    };

    return callCB;
}

module.exports = limitFunctionCallCount;