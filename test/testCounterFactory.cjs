const counterFactory = require ("../counterFactory.cjs");

const {increment, decrement} = counterFactory(10);

console.group("Increment")
console.log(increment())
console.log(increment())
console.log(increment())
console.groupEnd();

console.group("Decrement")
console.log(decrement())
console.log(decrement())
console.log(decrement())
console.groupEnd();
