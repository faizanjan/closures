const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

let n=1;
let callLimit = 5;
let cb = ()=>{console.log(n++)};
let result;

try {
    result = limitFunctionCallCount(cb, callLimit);
    for(let i=0; i<callLimit+3; i++){
        result();
    }
} catch (error) {
    console.error(error.message);
}