const cacheFunction = require("../cacheFunction.cjs");

let callback = (a,b)=>a*b;

const result = cacheFunction(callback);

if(result!==null){
    console.log(result(10,2))
    console.log(result(10,2))
    console.log(result(10,3))
    console.log(result(30,4))
    console.log(result(30,5))
    console.log(result(30,4))
}