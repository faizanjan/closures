function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.

    if(typeof(cb)!=='function') {
        throw new Error("Invalid Parameters");
    }
    
    let cache = {};

    let cachingFunc = (...arg)=>{
        let strArg = JSON.stringify(arg);
        let cachedArgs = Object.keys(cache);
        if(!cachedArgs.includes(strArg)){
            cache[strArg]=cb(...arg);
            console.log("New Arguments");
        }
        console.log(cache);
        return cache[strArg];
    }
    return cachingFunc;
}

module.exports = cacheFunction;